# == Class: duoproxy::apt
#
# APT configuration for Duo Proxy systems
#
# === Authors
#
# Xueshan Feng <sfeng@stanford.edu>
# Scotty Logan <swl@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2016-2017 The Board of Trustees of the Leland Stanford
# Junior University
#
class duoproxy::apt {

  class { 'apt':
    purge   => {
      'sources.list'   => false,
      'sources.list.d' => false,
    },
  }

  # force apt-get update before package installation
  Class['apt::update'] -> Package<| |>

}
