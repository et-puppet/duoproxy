# Class: duoproxy
# ===========================
#
# configure an Emerging Technology Duo Proxy
#
# === Authors
#
# Scotty Logan <swl@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2017 The Board of Trustees of the Leland Stanford Junior
# University
#
class duoproxy {

  class { 'duoproxy::packages': }
  -> class { 'duoproxy::build': }

}

