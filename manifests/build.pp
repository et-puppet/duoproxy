# == Class: duoproxy::build
#
# Build the Duo Proxy from source
#
# === Authors
#
# Scotty Logan <swl@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2017 The Board of Trustees of the Leland Stanford Junior University
#
class duoproxy::build (
  $tar_url,
  $python,
  $cleanup1,
  $cleanup2,
  $install_dir,
  $service_user,
  $log_group,
) {

  $build_dir = '/tmp/build'

  file { 'build-dir':
    ensure => directory,
    path   => $build_dir,
  }

  exec { 'unpack duoproxy':
    command => "/usr/bin/curl -s ${tar_url} | /bin/tar --strip-components=1 -xzf -",
    cwd     => $build_dir,
    creates => "${build_dir}/Makefile",
    require => [
      File['build-dir'],
    ],
    notify  => Exec['build'],
  }

  exec { 'build':
    command     => '/usr/bin/make',
    cwd         => $build_dir,
    timeout     => 0,
    environment => [ "PYTHON=${python}" ],
    creates     => "${build_dir}/duoauthproxy-build",
    notify      => Exec['install'],
  }

  exec { 'install':
    command => join([
      "${build_dir}/duoauthproxy-build/install",
      "--install-dir=${install_dir}",
      "--service-user=${service_user}",
      "--log-group=${log_group}",
      '--create-init-script=yes'
    ], ' '),
    cwd     => "${build_dir}/duoauthproxy-build",
    creates => '/opt/duoauthproxy/uninstall',
    notify  => Exec['remove build-dir'],
  }

  exec { 'remove build-dir':
    command => "/bin/rm -rf ${build_dir}",
    cwd     => '/',
    notify  => Exec['cleanup1'],
  }

  exec { 'cleanup1':
    command => $cleanup1,
    cwd     => '/',
    notify  => Exec['cleanup2'],
  }

  exec { 'cleanup2':
    command => $cleanup2,
    cwd     => '/',
  }

}
