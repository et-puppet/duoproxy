# == Class: duoproxy::packages
#
# Installs / updates packages needed for Duo Proxy
#
# === Authors
#
# Xueshan Feng <sfeng@stanford.edu>
# Scotty Logan <swl@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2017 The Board of Trustees of the Leland Stanford Junior
# University
#
class duoproxy::packages (
  $install,
  $uninstall,
  $package_type,
){

  include stdlib

  Package { provider => $package_type }

  class { "${module_name}::${package_type}": }

  $only_install = difference($install, $uninstall)

  $installing = join($only_install, ' ')
  $uninstalling = join($uninstall, ' ')

  ensure_packages($only_install, {
    ensure => present
  })

  ensure_packages($uninstall, {
    ensure => absent
  })

}
